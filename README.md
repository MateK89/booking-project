# booking-testproject

Made by M�t� Kert�sz on 2019.03.17
Used Angular latest to implement the input field.
I used rxJS to request for the API data.

# Running the application

- run npm install in input-box-app folder
- run ng serve
- if angular/@cli is not available globaly it needs to be installed to make the application compile
