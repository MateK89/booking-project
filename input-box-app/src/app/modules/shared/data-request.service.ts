import { Injectable } from '@angular/core';
import { Observable, from, of  } from 'rxjs';
import { map, catchError, } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

@Injectable({
  providedIn: 'root'
})
export class DataRequestService {
  public requestEndPoint (url:string) {
    return ajax(url).pipe(map(res => {
    if (!res.response) {
      throw new Error('Value expected!');
    }
    return res.response;
  }),
  catchError(err => of([])))
  }
}
