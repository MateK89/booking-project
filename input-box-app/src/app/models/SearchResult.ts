

export interface IResult {
  docs: IDocItem[];
  isGooglePowered: boolean;
  numFound: number;
}

export interface ISearchResult {
  results: IResult
}

export interface IDocItem {
  alternative: any[];
  bookingId: string;
  city: string;
  country: string;
  countryIso: string;
  iata: string;
  isPopular: boolean;
  lang: string;
  lat: number;
  lng: number;
  locationId: string;
  name: string;
  placeKey: string;
  placeType: string;
  region: string;
  searchType: string;
  ufi: number;
}
