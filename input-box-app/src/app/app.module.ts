import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { PickupLocationInputBoxModule } from './components/pickup-location-input-box/pickup-location-input-box.module';
import { SharedModule } from './modules/shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SharedModule.forRoot(),
    PickupLocationInputBoxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
