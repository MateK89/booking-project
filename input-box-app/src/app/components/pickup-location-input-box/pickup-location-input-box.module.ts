import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PickupLocationInputBoxComponent } from './pickup-location-input-box/pickup-location-input-box.component';
import { FormsModule }   from '@angular/forms';
import { ResultListComponent } from './result-list/result-list.component';
@NgModule({
  declarations: [PickupLocationInputBoxComponent, ResultListComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [ PickupLocationInputBoxComponent, ResultListComponent]
})
export class PickupLocationInputBoxModule { }
