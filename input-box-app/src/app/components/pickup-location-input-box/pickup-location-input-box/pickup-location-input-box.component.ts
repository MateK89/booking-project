import { Component, OnInit } from '@angular/core';
import { DataRequestService } from '../../../modules/shared/data-request.service';
import { ISearchResult } from '../../../models/SearchResult';
import { IDocItem } from '../../../models/SearchResult';
import { Observable } from 'rxjs';


const SearchCredentialStatics = {
  DATA_REQUEST_BASE_URL: (numberOfResults:number, searchItem:string) => {
    return `https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=${numberOfResults}&solrTerm=${searchItem}`},
  MINIMAL_INPUT_VALUE_LENGTH: 3
}


@Component({
  selector: 'app-pickup-location-input-box',
  templateUrl: './pickup-location-input-box.component.html',
  styleUrls: ['./pickup-location-input-box.component.scss']
})

export class PickupLocationInputBoxComponent implements OnInit {

  public inputValue:string;
  canShowResults: boolean = false;
  public result$: Observable<ISearchResult>;
  dataRequestService:DataRequestService;

  constructor(dataRequestService: DataRequestService) {
    this.dataRequestService = dataRequestService;
  }

  ngOnInit() {
  }

  onInputChange () {
    const mimimalInputValueLength: number = SearchCredentialStatics.MINIMAL_INPUT_VALUE_LENGTH;
    if(this.inputValue.length >= mimimalInputValueLength) {
      this.canShowResults = true;
      this.result$ = this.dataRequestService.requestEndPoint(SearchCredentialStatics.DATA_REQUEST_BASE_URL(6, this.inputValue));
      this.result$.subscribe();
    } else {
      this.canShowResults = false;
    }
  }

}
