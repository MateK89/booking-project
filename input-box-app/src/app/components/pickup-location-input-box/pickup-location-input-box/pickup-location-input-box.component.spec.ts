import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupLocationInputBoxComponent } from './pickup-location-input-box.component';

describe('PickupLocationInputBoxComponent', () => {
  let component: PickupLocationInputBoxComponent;
  let fixture: ComponentFixture<PickupLocationInputBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickupLocationInputBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupLocationInputBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
