import { Component, OnInit, Input} from '@angular/core';
import { IDocItem } from '../../../models/SearchResult';
@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.scss']
})
export class ResultListComponent implements OnInit {

  @Input() results: IDocItem;
  @Input() searchValue: string;

  constructor() { }

  ngOnInit() {
  }

  getTextFor(text:string) {
    const find:string = this.searchValue;
    let re = new RegExp(find, 'g');
    return text.replace(re, '<b>'+find+'</b>');
  }

}
